# Docker CI/CD
This project contains all Descriptions and settings necesarry for the deployment of an CI/CD chain, containing the following elements:
- [Docker](https://www.docker.com/) for running all applications in containers
- [Portainer](https://www.portainer.io/)  for managing the containers
- [Gitlab](https://about.gitlab.com) utilitizing all Git, Issue Management and a CI pipeline
- A [nginx](https://www.nginx.com/) reverse proxy for securing everything with SSL
- [SonarQube](https://www.sonarqube.org/) for checking code quality and security issues in our project
- ...more to come!

I am far from beeing an expert. If you find anything wrong, could be enhanced or not working feel free to [contact me](mailto:julian.sauer@outlook.com)!

## Prerequisites
All you need to start is a server and an operating system. For me worked
- A server with 2 cores, 8GB RAM and 80GB SSD
- As operating system: Ubuntu 18.04 LTS (*For future reference: When I speak about the **host**, I mean this operating system*)

**Important** Some of the comming console commands may need admin privileges. Make sure to have them on you operating system. Also if there are several versions (e.g. free community or paid enterprise version) I'm always choosing the free version. That's why I skip steps where you have to do special tasks to choose your version. The official documentations will more specific instructions.

I will explan every command line parameter and other variables once. If you see one that's no explained it will be found some time earlier in the file (unless I really forgot it).
## Installing Docker
The start is pretty easy and follows the instructions from the [Docker homepage](https://docs.docker.com/install/linux/docker-ce/ubuntu/).

Lets start by updating apt:

```bash
$ sudo apt-get update
```
Then enable the system to pull from an repository via https:

```bash
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```
Now we add Docker´s official GPG key:

```bash
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```
After the key we add the (stable) repository:

```bash
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```

As last step before the actual install we update apt once more:

```bash
$ sudo apt-get update
```

Finally we can install docker:

```bash
$ sudo apt-get install docker-ce docker-ce-cli
```


The ce stands for community edition.

And that is it! The first steps are done!
## Installing Docker Compose
Because it is great and we need it later with Portainer, we will also need [Docker Compose](https://docs.docker.com/compose/) for our project.

First we need to download the newest stable release:

```bash
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```
Make the binary executable:

```bash
$ sudo chmod +x /usr/local/bin/docker-compose
```
That was already it!

## Installing Portainer

To make everything visually more apprealling we use [Portainer](https://www.portainer.io/). Now we use docker for the first time. But this is also the point where things get a little complitcated and need to be explained.

First we create a volume :

```bash
$ sudo docker volume create portainer_data
```
You'll find everything about volumes [here](https://docs.docker.com/storage/volumes/). To make it short: This is a place where the image will persist data. This volume exists on our host. `portainer_data` ist the name of the volume. You can freely choose how to call it.

We will need it in our next step:

```bash
$ sudo docker run -d -p 8000:8000 -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
```
(If you don't understand something or want to know more check out the [official documentation for docker run](https://docs.docker.com/engine/reference/run/))
That is a lot to swallow. Let's look at it step by step:
- `docker run` tells docker to run an image inside a container.
- `-d` makes the container run detached in the background.
- `-p` maps a port of the host system to a port of the container. Those are made in the sheme `host-port:container-port`. In this case the command `-p 8000:8000`, everything arriving on port 8000 will be forwarded to port 8000 in the container. The same goes for port 9000. Later there will be cases where those port numbers are not the same.
- `-v` maps a directory or file of the host system to one in the container. The sheme is again `host-path:container-path`. Non existing directories will be automatically created. So in the this case `-v /var/run/docker.sock:/var/run/docker.sock` maps the docker socket of the host to the one in the container. This means, that `/var/run/docker.sock` in the container and `/var/run/docker.sock` inside the host **are the same**. Without `-v` those would be **two seperate files**, complete independent of each other.
- `portainer/portainer` describes the image that should be used inside the container. The [portainer image](https://hub.docker.com/r/portainer/portainer) comes from [Dockerhub](https://hub.docker.com). Everything that comes after the image name are commands for the immage itself.

And finish! you can now visit Either your IP-Adress `123.456.789.666:9000` or if you have a domain set up `your.domain.toplevel:9000` and you should see the Portainer side. Enter the admin password and you are ready to go.

### Securing portainer without reverse proxy

If you want to secure you portainer without the reverse proxy that comes next you can do everything like before except the last command. That needs to be tweaked here a little:

```bash
$ sudo docker run -d -p 80:8000 -p 443:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data -v /etc/ssl/portainer:/certs portainer/portainer --ssl --sslcert /certs/SSL-CERTIFICATE.cer --sslkey /certs/private/CERTIFICATE_PRIVATE_KEY.key
```
What's different? 
- Other port mappings: `-p 80:8000 -p 443:9000` standard http and https request to the host (port 80 and 443) will be forwarded to the corresponding ports of portainer
- Addiotional volume bind: `-v /etc/ssl/portainer:/certs` this is the place where you have saved the ssl certificate and key on your host. Both places can be freely decided by you. It's just important that you place the certificate and private key inside on the host.
- `--ssl` enables ssl on nginx that runs inside the image
- `--sslcert /certs/SSL-CERTIFICATE.cer` describes where the ssl certificate is placed inside the container. Since I placed the certificate inside `/etc/ssl/portainer` but mapped that directory to the `/certs` of my container it finds inside the file inside `/certs`. The file can be either a `.cer`, `.pem` or `.crt`.
- `--sslkey /certs/private/CERTIFICATE_PRIVATE_KEY.key` same like the certificate but with the private key

That's it! Your portainer should be now be reaced under your IP-Adress `http://123.456.789.666` or if you have a domain set up `https://your.domain.toplevel`.

## Installing Gitlab

Now this is where the fun begins. Let us start to [use Gitlab](https://docs.gitlab.com/omnibus/docker/). First we need to start the image in a container. But before that, we gonna take a look what Gitlab actually uses:
- A SQL database (like Postgres or MySQL)
- [Redis](https://redis.io/), another Database, but it's in-memory (as a key value store)
- The actual frontend

We *could* just use the frontend image, because it contains a database and redis, but there is a downside: No data would be saved on container reboot. That's where Docker Compose and Portainer come to action. We could start all those images via command line, but that would be unmanagble. Also we need to remember all services that depend on each other. It would be better to have them all on one look. To reach that goal, login in to your Portainer instance we started earlier. Once arrived, click on **Stacks** (either on the left or on the dashboard):

![Click Stacks](/images/gitlab_install/gitlab_1.png)

Once there click on **Add stack** to get things working.

![Click Add Stack](/images/gitlab_install/gitlab_2.png)
What is a stack actually? It's a config file for several images that get managed together. As written at the start of this chapter, we have three components for this to work. So our compose file (or stack) contains three images. One for each of the components (database, Redis and the frontend).
Give it any name you want (I recomment gitlab) and use the web editor to insert to correct settings. Or you can use the [prepared file](compose_files/gitlab-compose.yml) and upload it.

The content of the file should look like this:

```yml
version: '2'
services:
  db-gitlab:
    image: postgres:10.10
    restart: always
    hostname: postgres-gitlab
    container_name: postgres-gitlab
    environment:
      - POSTGRES_USER=gitlab
      - POSTGRES_PASSWORD=gitlab
      - POSTGRES_DB=gitlab
    networks:
      - gitlab
  redis:
    image: redis:latest
    restart: always
    hostname: redis
    networks:
      - gitlab
  web:
    image: gitlab/gitlab-ce:12.3.4-ce.0
    container_name: 'gitlab'
    restart: always
    hostname: 'gitlab.yggdra.co'
    ports:
      - "80"
      - "4430:443"
      - "2200:22"    
    environment: 
      GITLAB_OMNIBUS_CONFIG: |        
        external_url 'https://gitlab.yggdra.co'
        postgresql['enable'] = false
        gitlab_rails['gitlab_shell_ssh_port'] = 2200
        gitlab_rails['db_username'] = "gitlab"
        gitlab_rails['db_password'] = "gitlab"
        gitlab_rails['db_host'] = "postgres-gitlab"
        gitlab_rails['db_port'] = "5432"
        gitlab_rails['db_database'] = "gitlab"
        gitlab_rails['db_adapter'] = 'postgresql'
        gitlab_rails['db_encoding'] = 'utf8'
        gitlab_rails['backup_path'] = '/etc/backup'
        gitlab_rails['backup_keep_time'] = 604800
        redis['enable'] = false
        gitlab_rails['redis_host'] = 'redis'
#Uncomment this if you want to let gitlab hand out it's ssl certificates	  		
#        nginx['ssl_certificate'] = "/certs/YOUR_CERTIFICATE_FILE.pem"
#        nginx['ssl_certificate_key'] = "/certs/private/YOUR_PRIVATE_CERTIFICATE_KEY.key"
#        nginx['redirect_http_to_https'] = true
    networks:
      - gitlab
    volumes:
      - ./gitlab/conf:/etc/gitlab
      - ./gitlab/logs:/var/log/gitlab
      - ./gitlab/main:/var/opt/gitlab
      - /home/backups-gitlab:/etc/backup
#Uncomment this if you want to let gitlab hand out it's ssl certificates	  
#      - /etc/ssl/gitlab:/certs
```

That's a lot to swallow. But don't worry, you actually know the basics already.

- `version` defines the version of the docker-compose syntax. As I'm writing this, version 3 is already released, but portainer only supports version 2. In case for this tutorial, there is no harm in using version 2.
- `services` defines the services (or container/images) that run inside this stack. Since we have 3 components, we have 3 components, we also have 3 services. Each representing one component. You can choose the name of the services freely.

That's it for the general definition. For better clarity, we look at each service one by one.

```yml
db-gitlab:
    image: postgres:10.10
    restart: always
    hostname: postgres-gitlab
    container_name: postgres-gitlab
    environment:
      - POSTGRES_USER=gitlab
      - POSTGRES_PASSWORD=gitlab
      - POSTGRES_DB=gitlab
    networks:
      - gitlab
```

This part defines the database.
- `image` defines the dockerhub image we use. As you can see we use a [PostgresSQL](https://hub.docker.com/_/postgres) database.
- `restart` defines the logic we use if something with the image goes wrong (or docker starts, eg. after an reboot). In this case we (re)start everytime.
- `hostname` defines the name other containers can reach this one (as long as they share a network).
- `container_name` defines the display name of the container (eg. in docker or portainer).
- `environment` defines (and overrides existing) variables for the container. In this case we define a user, password and the nam of the db to initialize. As you can see, the are currently important data in clear text. This is for making this easier to understand. [**There are ways to mask them!**](https://docs.docker.com/engine/swarm/secrets/)
- `network` defines a network to join. You can choose the name freely. Just make sure that containers that shall reach it other join the same network (like in this file). If the networks doesn't exist already, it will be created automatically.

```yml
redis:
    image: redis:latest
    restart: always
    hostname: redis
    networks:
      - gitlab
```

This is the Redis instance. There is nothing much to say here. All properties have already been explained.

```yml
    image: gitlab/gitlab-ce:12.3.4-ce.0
    container_name: 'gitlab'
    restart: always
    hostname: 'gitlab.yggdra.co'
    ports:
      - "8001:80"
      - "4430:443"
      - "2200:22"    
    environment: 
      GITLAB_OMNIBUS_CONFIG: |        
        external_url 'https://gitlab.yggdra.co'
        postgresql['enable'] = false
        gitlab_rails['gitlab_shell_ssh_port'] = 2200
        gitlab_rails['db_username'] = "gitlab"
        gitlab_rails['db_password'] = "gitlab"
        gitlab_rails['db_host'] = "postgres-gitlab"
        gitlab_rails['db_port'] = "5432"
        gitlab_rails['db_database'] = "gitlab"
        gitlab_rails['db_adapter'] = 'postgresql'
        gitlab_rails['db_encoding'] = 'utf8'
        gitlab_rails['backup_path'] = '/etc/backup'
        gitlab_rails['backup_keep_time'] = 604800
        redis['enable'] = false
        gitlab_rails['redis_host'] = 'redis'
#Uncomment this if you want to let gitlab hand out it's ssl certificates	  		
#        nginx['ssl_certificate'] = "/certs/YOUR_CERTIFICATE_FILE.pem"
#        nginx['ssl_certificate_key'] = "/certs/private/YOUR_PRIVATE_CERTIFICATE_KEY.key"
#        nginx['redirect_http_to_https'] = true
    networks:
      - gitlab
    volumes:
      - ./gitlab/conf:/etc/gitlab
      - ./gitlab/logs:/var/log/gitlab
      - ./gitlab/main:/var/opt/gitlab
      - /home/backups-gitlab:/etc/backup
#Uncomment this if you want to let gitlab hand out it's ssl certificates	  
#      - /etc/ssl/gitlab:/certs
```

This covers the actual Gitlab instance. As you can see some ports on the host machine get mapped to some ports of the container. Some folders of the host machine get bound to the container. Note: The volume bind `- /home/backups-gitlab:/etc/backup` is for creating backups and is not necessary for the container to work.

Important for this image are the initial gitlab config settings. Those are set inside the `GITLAB_OMNIBUS_CONFIG` and are used for the following seetings.
- `external_url` describes how the url looks for the outside user of this gitlab instance. For example: If you configured a reverse proxy that redirects gitlab.your-domain.com to your gitlab under your-domain.com:1234 then you have to set the external_url to "gitlab.your-domain.com" because thats what you user sees.
- `postgresql['enable'] = false` this disables the shipped postgressql database (because we use our own).
- `gitlab_rails['db_username'] = "gitlab"` sets the username for gitlab to user of our postgres database. Needs to match the corresponding environment variable of the postgres database container.
- `gitlab_rails['db_password'] = "gitlab"` sets the password for gitlab to user of our postgres database. Needs to match the corresponding environment variable of the postgres database container.
- `gitlab_rails['db_host'] = "postgres-gitlab"` sets the host for gitlab to user of our postgres database. Remember that we used `postgres-gitlab` as hostname for the database image and joined the `gitlab`-network on this and the database? That's why we did it.out gitlab container finds the postges-gitlab container because of this.
- `gitlab_rails['db_port'] = "5432"` is the default port of the database, no need to change it. Because they both joined that same network (gitlab) we don't need to expose any hostports. They can communicate with each other as if they were on seperate systems.
- `gitlab_rails['db_database'] = "gitlab"` is the name of the database. Needs to match the corresponding environment variable of the postgres database container.
- `gitlab_rails['db_adapter'] = 'postgresql'` tells gitlab which kind of database we use so it uses the correct adapter to connect with.
- `gitlab_rails['db_encoding'] = 'utf8'` is the encoding of the database. No need to change it.
- `gitlab_rails['backup_path'] = '/etc/backup'` tells gitlab where to save it's database backup. Only needed if you want to use the backup function of gitlab.
- `gitlab_rails['backup_keep_time'] = 604800` tells gitlab how long to keep backup files in the folder. In this case it's 7 days.

If you don't want to use a reverse proxy and let gitlab (to be precise the nginx server inside the gitlab image) handle ssl certificates add the following to the `GITLAB_OMNIBUS_CONFIG`:

- `nginx['ssl_certificate'] = "/certs/YOUR_CERTIFICATE_FILE.pem"` describes where the ssl certificate is placed inside the container. The file can be either a `.cer`, `.pem` or `.crt`.
- `nginx['ssl_certificate_key'] = "/certs/private/YOUR_PRIVATE_CERTIFICATE_KEY.key"` same as with the certificate but with the key.
- `nginx['redirect_http_to_https'] = true` tells gitlab to redirect any unsercure http request to secure https.

Make sure to also bind the volume that contains the certificate and key file on your host to the container path you use for those properties.

After that you are all set! Press **"Deploy the stack"** at the end of the page and let portainer do the work. After some seconds (depending on the power and dl-speed of the host) you'll get redirected to the stack overview where you can see your containers running.
Note: Gitlab is complex and needs some time to start. The container takes 1-5 minutes to start. So don't worry when the image status displays "starting" for several minutes. After a while it should display "running".

Once everything is started you stack should look like this (Ignore the blacked out lines, our stack gets bigger in the next steps):

![Stack overview](/images/gitlab_install/gitlab_3.png)

Depending on your settings you can reach the gitlab instance now under your-ip:8001, your-ip:4430, your-domain:8001 or your-domain:4430.
The image has a default admin account with username:password of `root:root`. **Make sure to change it immediately!**

## Adding a runner

Now that our gitlab is running, we just need a gitlab runner to make our CI work. First we got to add the runner to our portainer stack. for that we edit the stacks compose file directly through portainer. Login to your portainer instance, navigate to **"stacks"** and click on your gitlab stack. 

![Edit the stack](/images/gitlab_runner/runner_0.png)

Once inside the editor, add the following at the end of the file ([you can also find the content of the whole compose file here](compose_files/gitlab-and-runner-compose.yml)):

```yml
  runner:
    image: gitlab/gitlab-runner:alpine-v12.1.0
    container_name: 'gtitlab-runner'
    restart: always
# Uncomment if you want to start the runner in debug mode
#    environment:
#      - DEBUG=true
    volumes:     
     - ./config-runner:/etc/gitlab-runner
     - /var/run/docker.sock:/var/run/docker.sock
# Uncomment if you need to provide your runner with a ca file
#     - /etc/ssl/gitlab:/etc/ssl/certs
    depends_on:
      - web
    networks: 
      - gitlab
```

This adds the image of the gitlab runner to our stack. Besides the known configurations, there is new ones:
- `depends_on` makes sure to start the runner after the web interface has been startet. Also it stops the web before the runner. But it doesn't wait for the web to fully start the runner. For more read [in the documentation](https://docs.docker.com/compose/compose-file/compose-file-v2/).

Also you can start the runner in debug mode if you set the corresponding environment variable.
If yor runner needs *additional* certificates (e.g. an root certificate, because yours doesn't contain the whole chain) you can bin the containing folger here. More on that topic in the FAQ section.

Now it's time to register a runner in gitlab.

When you are done, save the changes by pressing **"Update the stack"**. After some seconds you should be redirected to the stack and see that you services have startet or are starting right now. There should be a new one: "gitlab-runner".

Once all services have finished starting, log in to ypur gitlab instance as the root or any admin user you may have created. Navigate to the administration settings:

![Administration settings](/images/gitlab_runner/runner_1.png)

Go to the runner section:

![Open runner overview](/images/gitlab_runner/runner_2.png)

Here all of your runners are shown (currently none):

![Runner overview](/images/gitlab_runner/runner_3.png)

Stay on that page (You need the token in the upper right corner later) and move over to your host.
Execute the following command:

```bash
$ sudo docker exec gitlab-runner gitlab-runner register -n --url=YOUR_URL --registration-token=YOUR_GITLAB_TOKEN --name=runner --run-untagged=true --locked=false --executor=docker --docker-image=alpine:latest
```

- `docker exec gitlab-runner` this means docker shall execute a command on the container we called "gitlab-runner". If you choose any other container name for gitlab-runner, make sure to change it accordingly.
- `gitlab-runner` are the basic commands for the runner. **So it's no mistake that there are two "gitlab-runner" in this command.**
- `register` this registers a new runner.
- `-n` stands for "non-interactive". We could also register in interactive mode, where we get asked all settings one by one. But the whole command has all it needs so we can skip that.
- `--url` is the url of the gitlab-instance. This value must be the same as `external_url` if the gitlab settings.
- `--registration-token` In your gitlab runner overview in the top right (see the red square in the last image). This is the token you need. 
- `--name=runner` The name of the runner. Choose whatever you like.
- `--run-untagged=true` makes this runner available for all possible jobs. You can change this later inside gitlab. If you wanna know whats possible [look here](https://docs.gitlab.com/ee/ci/runners/#using-tags).
- `--locked=false` makes the runner available to all projects. For more information [look here](https://docs.gitlab.com/ee/ci/runners/#locking-a-specific-runner-from-being-enabled-for-other-projects).
- `--executor=docker` makes the runner use a docker container to run the pipeline inside. For all possible executors [look here](https://docs.gitlab.com/runner/executors/).
- `--docker-image=alpine:latest` defines what image for the docker container shall be used, if no other is given by the configuration file for the job.

There should be some output ending in 
``
Registering runner... succeeded                     runner=SOME_TOKEN
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
``

Now the runner should be visible inside the runner configuration in gitlab:

![A runner appeared](/images/gitlab_runner/runner_4.png)

Congratulations! Now you have a working runner!